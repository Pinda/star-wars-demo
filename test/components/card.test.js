/* eslint-env jest */
import React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";

import Card from "../../src/components/card";

describe("<Card />", () => {
  it("render Card", () => {
    const planet = ["url/2"];
    const wrapper = shallow(<Card planet={planet} />);
    expect(wrapper.find(".collapsible-header")).to.have.length(1);
  });
});
