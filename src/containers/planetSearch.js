import React, { Component } from "react";
import _ from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchPlanets } from "../actions";

import FilterPlanetContainer from "./filterPlanet";
import Loader from "../components/loader";

class PlanetList extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  renderList = planet => {
    // to get correct index after the filtering

    let stableIndex;

    this.props.planets.forEach((nonFilteredPlanet, i) => {
      if (_.isEqual(nonFilteredPlanet, planet)) {
        stableIndex = i;
      }
    });

    return (
      <tr key={planet.created}>
        <td>{planet.name}</td>
        <td>{planet.population}</td>
        <td>{planet.terrain}</td>
        <td>
          <button
            className="btn waves-effect waves-light"
            onClick={() => this.viewDetails(planet, stableIndex)}
          >
            View
          </button>
        </td>
      </tr>
    );
  };

  viewDetails(planet, index) {
    this.props.router.push(`/planet/${index}`);
  }

  displayPlanets = () => {
    const { name, population, terrain } = this.props.filterValue;

    let filteredValues = this.props.planets;

    // sort for name
    if (this.props.sortValue !== "" && this.props.sortValue.name) {
      filteredValues = this.sortPlanet(
        this.props.sortValue.name,
        "name",
        filteredValues
      );
    }

    // sort for population
    if (this.props.sortValue !== "" && this.props.sortValue.population) {
      filteredValues = this.sortPlanet(
        this.props.sortValue.population,
        "population",
        filteredValues
      );
    }

    // sort for terrain
    if (this.props.sortValue !== "" && this.props.sortValue.terrain) {
      filteredValues = this.sortPlanet(
        this.props.sortValue.terrain,
        "terrain",
        filteredValues
      );
    }

    // executing filter for name
    if (name) {
      filteredValues = this.filterPlanets("name", name, filteredValues);
    }

    // executing filter for population
    if (population) {
      filteredValues = this.filterPlanets(
        "population",
        population,
        filteredValues
      );
    }

    // executing filter for terrain
    if (terrain) {
      filteredValues = this.filterPlanets("terrain", terrain, filteredValues);
    }

    return filteredValues.map(this.renderList);
  };

  filterPlanets(filterKey, filterValue, list) {
    const filteredList = _.filter(list, function(planet) {
      return _.includes(
        planet[filterKey].toLowerCase(),
        filterValue.toLowerCase()
      );
    });

    return filteredList;
  }

  sortPlanet(sortValue, field, list) {
    const sortedPlanets = _.sortBy(list, planet => {
      if (field === "population") {
        return parseInt(planet[field], 10);
      }
      return planet[field];
    });
    if (sortValue === "up") {
      return sortedPlanets;
    }
    return sortedPlanets.reverse();
  }

  componentWillMount() {
    this.props.fetchPlanets().then(() => {
      this.setState({ loading: false });
    });
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }

    return (
      <table className="striped">
        <thead>
          <FilterPlanetContainer />
        </thead>
        <tbody>{this.displayPlanets()}</tbody>
      </table>
    );
  }
}

function mapDispachToProps(dispatch) {
  return bindActionCreators({ fetchPlanets }, dispatch);
}

export default connect(
  ({ planets }) => ({
    planets: planets.listOfPlanets,
    filterValue: planets.filterValue,
    sortValue: planets.sortValue
  }),
  mapDispachToProps
)(PlanetList);
