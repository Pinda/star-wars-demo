import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router";

import Card from "../components/card";
import List from "./listNames";
import { fetchPlanets } from "../actions";
import Loader from "../components/loader";

class DisplayPlanet extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    // refetch data on reload
    this.props.fetchPlanets();
  }

  render() {
    if (this.props.listOfPlanets.length === 0) {
      return <Loader />;
    }

    const planet = this.props.listOfPlanets[this.props.routeParams.id];
    // pasing list of url to the List component
    const residentsUrl = planet.residents;

    return (
      <div>
        <Card planet={planet} />
        <List residents={residentsUrl} />
        <div className="back-button-wrapper">
          <Link to="/">
            <i className="material-icons dp48">arrow_back</i>
            <span>Back to planets</span>
          </Link>
        </div>
      </div>
    );
  }
}

function mapDispachToProps(dispatch) {
  return bindActionCreators({ fetchPlanets }, dispatch);
}

export default connect(
  ({ planets }) => ({
    listOfPlanets: planets.listOfPlanets
  }),
  mapDispachToProps
)(DisplayPlanet);
