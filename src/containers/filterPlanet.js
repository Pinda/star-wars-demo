import React, { Component } from "react";
import { connect } from "react-redux";
import { filterField, sort } from "../actions";

import SearchBlock from "../components/searchBlock";

class FilterPlanet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.filterValue.name,
      population: this.props.filterValue.population,
      terrain: this.props.filterValue.terrain
    };
  }

  handleInput = e => {
    const nameInput = e.target.name;
    const { value } = e.target;

    this.setState({
      [nameInput]: value
    });

    this.props.filterField({
      [nameInput]: value
    });
  };

  onSortUp = e => {
    const getName = e.target.getAttribute("name");
    this.props.sort({ [getName]: "up" });
  };

  onSortDown = e => {
    const getName = e.target.getAttribute("name");
    this.props.sort({ [getName]: "down" });
  };

  render() {
    return (
      <tr className="search-container">
        <SearchBlock
          name="name"
          value={this.state.name}
          onChangeFilter={this.handleInput}
          labelName="Name"
          onSortUp={this.onSortUp}
          onSortDown={this.onSortDown}
        />
        <SearchBlock
          name="population"
          value={this.state.population}
          onChangeFilter={this.handleInput}
          labelName="Population"
          onSortUp={this.onSortUp}
          onSortDown={this.onSortDown}
        />
        <SearchBlock
          name="terrain"
          value={this.state.terrain}
          onChangeFilter={this.handleInput}
          labelName="Terrain Details"
          onSortUp={this.onSortUp}
          onSortDown={this.onSortDown}
        />
      </tr>
    );
  }
}

export default connect(
  ({ planets }) => ({
    filterValue: planets.filterValue
  }),
  dispatch => ({
    filterField(searchValue) {
      return dispatch(filterField(searchValue));
    },
    sort(sortValue) {
      return dispatch(sort(sortValue));
    }
  })
)(FilterPlanet);
