import React, { Component } from "react";
import Axios from "axios";

import Loader from "../components/loader";

class List extends Component {
  constructor() {
    super();
    this.state = { residents: [], loading: true };
  }

  componentDidMount() {
    this.props.residents.forEach(residentUrl => {
      Axios.get(residentUrl).then(res => {
        this.setState({ residents: [...this.state.residents, res.data.name] });
        this.setState({ loading: false });
      });
    });
  }

  renderList = () => {
    if (this.props.residents.length === 0) {
      return <div className="no-found">No residents found</div>;
    }

    if (this.state.loading) {
      return <Loader />;
    }
    return (
      <div>
        {this.state.residents.map(resident => (
          <li className="collection-item" key={resident}>
            <i className="material-icons dp48">people</i>
            <div className="residents-item">{resident}</div>
          </li>
        ))}
      </div>
    );
  };

  render() {
    return (
      <ul className="collection with-header">
        <li className="collection-header">
          <h6>Names of the residents </h6>
        </li>
        {this.renderList()}
      </ul>
    );
  }
}

export default List;
