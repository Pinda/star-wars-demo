import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxPromise from "redux-promise";
import { Router, hashHistory } from "react-router";
import "materialize-css/dist/css/materialize.css";
import "materialize-css/dist/js/materialize";

import "../static/style.css";

import reducers from "./reducers";
import routes from "./router";

const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);

ReactDOM.render(
  <Provider
    store={createStoreWithMiddleware(
      reducers,
      window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__()
    )}
  >
    <Router history={hashHistory} routes={routes} />
  </Provider>,
  document.querySelector(".container")
);
