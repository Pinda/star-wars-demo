import { combineReducers } from "redux";
import PalnetReducer from "./reducer_planets";

const rootReducer = combineReducers({
  planets: PalnetReducer,
});

export default rootReducer;
