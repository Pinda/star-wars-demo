import { FETCH_PLANET, FILTER_PLANET, SORT_PLANET } from "../actions";

const INIT_STATE = {
  listOfPlanets: [],
  filterValue: "",
  sortValue: ""
};

export default function(state = INIT_STATE, action) {
  switch (action.type) {
    case FETCH_PLANET: {
      return {
        ...state,
        listOfPlanets: [...action.payload.data.results]
      };
    }

    case FILTER_PLANET: {
      return {
        ...state,
        filterValue: action.payload
      };
    }

    case SORT_PLANET: {
      return {
        ...state,
        sortValue: action.payload
      };
    }
    default:
      return state;
  }
}
