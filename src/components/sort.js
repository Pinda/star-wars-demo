import React from "react";

const Sort = props => {
  return (
    <div className="sort-container">
      <i role="button" className="material-icons" onClick={e => props.onSortUp(e)} name={props.name}>
        arrow_drop_up
      </i>
      <i role="button" className="material-icons" onClick={e => props.onSortDown(e)} name={props.name}>
        arrow_drop_down
      </i>
    </div>
  );
};

export default Sort;
