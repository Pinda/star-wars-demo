import React from "react";

const FilterInput = ({ name, value, onChangeFilter }) => {
  return (
    <div className="filter-container">
      <input name={name} value={value} onChange={e => onChangeFilter(e)} />
    </div>
  );
};

export default FilterInput;
