import React from "react";

const Footer = () => {
  return (
    <footer className="page-footer">
      <div className="container">
        <div className="row">
          <div className="col l6 s12">
            <h6 className="white-text">
              <i className="material-icons">email</i>
              <span className="email-footer">peto.pinda@gmail.com</span>
            </h6>
          </div>
        </div>
      </div>
      <div className="footer-copyright">
        <div className="container">© 2018 Copyright</div>
      </div>
    </footer>
  );
};

export default Footer;
