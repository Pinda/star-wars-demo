import React from "react";
import Navigation from "./navigation";
import Footer from "./footer";

const App = props => {
  return (
    <div className="nav-container">
      <Navigation location={props.location} />
      <div> {props.children}</div>
      <Footer />
    </div>
  );
};

export default App;
