import React from "react";
import { Link } from "react-router";

const Navigation = props => {
  return (
    <nav>
      <div className="nav-wrapper">
        <div className="col s12 nav-wrapper">
          <Link to="/" className="breadcrumb">
            Planets
          </Link>
          {props.location.pathname !== "/" && (
            <Link to="/" className="breadcrumb disabled-link">
              Planet details
            </Link>
          )}
        </div>
      </div>
    </nav>
  );
};

export default Navigation;
