import React from "react";

import FilterInput from "./filterInput";
import Label from "./label";
import Sort from "./sort";

const SearchBlock = props => {
  return (
    <th>
      <Label labelName={props.labelName} />
      <Sort
        name={props.name}
        onSortUp={props.onSortUp}
        onSortDown={props.onSortDown}
      />
      <FilterInput
        name={props.name}
        value={props.value}
        onChangeFilter={props.onChangeFilter}
      />
    </th>
  );
};

export default SearchBlock;
