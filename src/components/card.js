import React from "react";
import _ from "lodash";

const CardComponent = props => {
  const renderContent = () => {
    if (!props.planet) {
      return <div>No planet found</div>;
    }
    const updatedPlanet = _.omit(props.planet, ["residents", "films"]);
    return (
      <div>
        {Object.keys(updatedPlanet).map(item => (
          <li key={item}>
            <div className="collapsible-header">
              <i className="material-icons">filter_drama</i>
              <span className="item-key item">{item.toUpperCase()}:</span>
              <span className="item-value item">{props.planet[item]}</span>
            </div>
          </li>
        ))}
      </div>
    );
  };

  return <ul className="collapsible">{renderContent()}</ul>;
};

export default CardComponent;
