import Axios from "axios";

const ROOT_URL = "https://swapi.co/api/planets";

export const FETCH_PLANET = "FETCH_PLANET";
export const FILTER_PLANET = "FILTER_PLANET";
export const SORT_PLANET = "SORT_PLANET";

export function fetchPlanets() {
  const url = `${ROOT_URL}`;
  const request = Axios.get(url);

  return {
    type: FETCH_PLANET,
    payload: request
  };
}

export function filterField(searchValue) {
  return {
    type: FILTER_PLANET,
    payload: searchValue
  };
}

export function sort(sortValue) {
  return {
    type: SORT_PLANET,
    payload: sortValue
  };
}
