import React from "react";
import { Route, IndexRoute } from "react-router";
import App from "../components/app";
import PlanetList from "../containers/planetSearch";
import PlanetDetails from "../containers/displayPlanet";

export default (
  <Route path="/" component={App}>
    <IndexRoute component={PlanetList} />
    <Route path="planet/:id" component={PlanetDetails} />
  </Route>
);
